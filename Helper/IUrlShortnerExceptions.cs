using System;

namespace UrlShortner.Exceptions
{
    public interface IUrlShortnerException
    {
        
    }

    public class UrlShortnerException : Exception, IUrlShortnerException
    {
        public UrlShortnerException()
        {

        }

        public UrlShortnerException(string exceptionValue): base(exceptionValue)
        {

        }
    }
}