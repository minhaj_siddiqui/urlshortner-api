using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;

namespace UrlShortner.Helpers
{
    public class TinyUrlHelper : ITinyUrlHelper
    {
        public async Task<string> Encode(int Id)
        {
            var encodedUrlId = WebEncoders.Base64UrlEncode(BitConverter.GetBytes(Id));
            return await Task.FromResult<string>(encodedUrlId);
        }

        public async Task<int> Decode(string tinyUrlResource)
        {
            var decodedUrlId = WebEncoders.Base64UrlDecode(tinyUrlResource);
            return await Task.FromResult<int>(BitConverter.ToInt32(decodedUrlId));
        }
    }

}