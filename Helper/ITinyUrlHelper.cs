using System.Threading.Tasks;

namespace UrlShortner.Helpers
{
    public interface ITinyUrlHelper
    {
        Task<string> Encode(int Id);
        Task<int> Decode(string urlResource);

    }
}