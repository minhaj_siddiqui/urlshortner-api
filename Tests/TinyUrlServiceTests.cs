using Xunit;
using UrlShortner.Services;
using System.ComponentModel.DataAnnotations;
using Moq;
using UrlShortner.Helpers;
using UrlShortner.Data;
using UrlShortner.Models;
using Microsoft.EntityFrameworkCore;
using UrlShortner.Exceptions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace UrlShortner.TinyUrlServiceTest
{
    public class TinyUrlServiceTests
    {
        Mock<ITinyUrlHelper> _mockedTinyUrlHelper;
        Mock<IDbContext> _mockedContext;

        TinyUrlModel _tinyUrlModel;
        ITinyUrlService _tinyUrlService;


        public TinyUrlServiceTests()
        {
            _mockedTinyUrlHelper = new Mock<ITinyUrlHelper>();

            _mockedContext = new Mock<IDbContext>();

            _tinyUrlModel = new TinyUrlModel();

            _tinyUrlService = new TinyUrlService(_mockedTinyUrlHelper.Object, _mockedContext.Object);
        }

        [Fact]
        public void CreateTinyUrl_WithEmptyLongUrl_Throws_ValidationException()
        {
            // Arrange            
            _tinyUrlModel.LongUrl = "";

            // Act & Assert
            Assert.ThrowsAsync<ValidationException>(() => _tinyUrlService.CreateTinyUrl(_tinyUrlModel));
        }

        [Fact]
        public void CreateTinyUrl_WithNullLongUrl_Throws_ValidationException()
        {
            // Arrange
            _tinyUrlModel.LongUrl = null;

            // Act & Assert
            Assert.ThrowsAsync<ValidationException>(() => _tinyUrlService.CreateTinyUrl(_tinyUrlModel));
        }

        [Fact]
        public async void CreateTinyUrl_WithErrorInSavingUrl_InDb_ThrowsUrlShortnerException()
        {
            // Arrange
            _tinyUrlModel.LongUrl = "https://www.google.com";

            var listOfTinyUrlModel = new List<TinyUrlModel>();

            var mockedDbSet = new Mock<DbSet<TinyUrlModel>>();

            mockedDbSet.Setup(x => x.Add(_tinyUrlModel)).Callback<TinyUrlModel>((s) => listOfTinyUrlModel.Add(s));

            _mockedContext.Setup(x => x.TinyUrls).Returns(mockedDbSet.Object);

            _mockedContext.Setup(x => x.SaveChanges()).Returns(-1);

            // Act & Assert
            await Assert.ThrowsAsync<UrlShortnerException>(() => _tinyUrlService.CreateTinyUrl(_tinyUrlModel));
        }

        [Fact]
        public async void CreateTinyUrl_WithValidLongUrl_Returns_TinyURL()
        {
            // Arrange
            var expectedShortURI = "AABCCDD";

            _tinyUrlModel.LongUrl = "https://www.google.com";

            var listOfTinyUrlModel = new List<TinyUrlModel>();

            var mockedDbSet = new Mock<DbSet<TinyUrlModel>>();

            mockedDbSet.Setup(x => x.Add(_tinyUrlModel)).Callback<TinyUrlModel>((s) => listOfTinyUrlModel.Add(s));

            _mockedContext.Setup(x => x.TinyUrls).Returns(mockedDbSet.Object);

            _mockedContext.Setup(x => x.SaveChanges()).Returns(1);

            _mockedTinyUrlHelper.Setup(x => x.Encode(It.IsAny<int>())).Returns(Task.FromResult(expectedShortURI));

            // Act
            var actualShortURI = await _tinyUrlService.CreateTinyUrl(_tinyUrlModel);

            // Assert
            Assert.Equal(expectedShortURI, actualShortURI);
        }
    }
}