namespace UrlShortner.Messages
{
    public static class ErrorMessage
    {
        public static string UNKNOWN_ERROR = "Something went wrong.. Please try again.";
    }
}