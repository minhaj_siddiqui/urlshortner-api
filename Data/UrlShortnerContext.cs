using Microsoft.EntityFrameworkCore;
using UrlShortner.Models;

namespace UrlShortner.Data
{
    public class UrlShortnerContext : DbContext, IDbContext
    {
        public UrlShortnerContext(DbContextOptions<UrlShortnerContext> options) : base(options)
        {

        }

        public DbSet<TinyUrlModel> TinyUrls { get; set; }
    }

    public interface IDbContext
    {
        DbSet<TinyUrlModel> TinyUrls { get; set; }
        int SaveChanges();
    }

}