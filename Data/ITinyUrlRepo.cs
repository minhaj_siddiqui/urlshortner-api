using System.Collections.Generic;
using UrlShortner.Models;

namespace UrlShortner.Data
{
    public interface ITinyUrlRepo
    {
        IEnumerable<TinyUrlModel> GetAllTinyUrl();
    }
}