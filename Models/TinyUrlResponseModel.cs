namespace UrlShortner.Models
{
    public class TinyUrlResponseModel
    {

        public TinyUrlResponseModel(string url)
        {
            tinyUrl = url;
        }

        public string tinyUrl {set; get;}
    }
}