using System.ComponentModel.DataAnnotations;

namespace UrlShortner.Models
{
    public class TinyUrlModel
    {
        public int Id {get; set;}

        [Required]
        public string LongUrl {get; set;}

        public string ShortUrl {get; set;}
    }
}