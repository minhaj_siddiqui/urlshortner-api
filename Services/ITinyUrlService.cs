using System.Threading.Tasks;
using UrlShortner.Models;

namespace UrlShortner.Services
{
    public interface ITinyUrlService
    {
        Task<string> CreateTinyUrl(TinyUrlModel tinyUrlModel);

        Task<string> GetOriginalUrl(string tinyUrlResource);
    }
}