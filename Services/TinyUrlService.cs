using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UrlShortner.Data;
using UrlShortner.Exceptions;
using UrlShortner.Helpers;
using UrlShortner.Messages;
using UrlShortner.Models;

namespace UrlShortner.Services
{
    public class TinyUrlService : ITinyUrlService, IUrlShortnerException
    {       
        ITinyUrlHelper _tinyUrlHelper;
        private readonly IDbContext _context;

        public TinyUrlService(ITinyUrlHelper tinyUrlHelper, IDbContext context)
        {
            _tinyUrlHelper = tinyUrlHelper;
            _context = context;
        }

        public async Task<string> CreateTinyUrl(TinyUrlModel tinyUrlModel)
        {
            if(tinyUrlModel.LongUrl == null || tinyUrlModel.LongUrl == string.Empty)
            {
                throw new ValidationException("Url Can not be Null or Empty");
            }

            // This is ensure that we always add http by default if no protocol is passed.
            if(!tinyUrlModel.LongUrl.StartsWith("https://") & !tinyUrlModel.LongUrl.StartsWith("http://"))
            {
                tinyUrlModel.LongUrl = "http://" + tinyUrlModel.LongUrl;
            }

            _context.TinyUrls.Add(tinyUrlModel);
            
            var result = _context.SaveChanges();

            if (result <=0)
            {
                throw new UrlShortnerException(ErrorMessage.UNKNOWN_ERROR);
            }

            var tinyUrl = await _tinyUrlHelper.Encode(tinyUrlModel.Id);

            return  tinyUrl;
        }

        public async Task<string> GetOriginalUrl(string tinyUrlResource)
        {
            if(tinyUrlResource == null || tinyUrlResource == string.Empty)
            {
                throw new ValidationException("Tiny url Can not be Null or Empty");
            }

            var decodedUrlId = await _tinyUrlHelper.Decode((tinyUrlResource));

            var result = _context.TinyUrls.Find(decodedUrlId);
            
            return await Task.FromResult<string>(result.LongUrl);
        }
    }

}