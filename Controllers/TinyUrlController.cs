using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using UrlShortner.Models;
using UrlShortner.Services;

namespace UrlShortner.Controllers
{
    [ApiController]
    // [Route("api/tinyUrl")]
    [Produces("application/json")]
    public class TinyUrlController : Controller
    {
        private readonly ITinyUrlService _service;

        public TinyUrlController(ITinyUrlService service)
        {
            _service = service;
        }

        [HttpGet("{path:required}")]
        public async Task<IActionResult> RedirectToOriginalUrl(string path)
        {
            if (path == null)
            {
                return NotFound();
            }

            var originalUrl = await _service.GetOriginalUrl(path);

            if (originalUrl == null)
            {
                return NotFound();
            }

            return Redirect(originalUrl);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateTinyUrl(TinyUrlModel tinyUrlModel)
        {
            TryValidateModel(tinyUrlModel); 

            if (!ModelState.IsValid)
            {
                return ValidationProblem();
            }

            // Test our URL
            if (!Uri.TryCreate(tinyUrlModel.LongUrl, UriKind.Absolute, out Uri result))
            {
                return BadRequest("Please check URL since it does not match with the Url Pattern.");
            }

            var tinyResource = await _service.CreateTinyUrl(tinyUrlModel);

            var responseUrl = $"{Request.Scheme}://{Request.Host}/{tinyResource}";
            
            tinyUrlModel.ShortUrl = responseUrl;

            return Ok(tinyUrlModel);
        }
    }
}